import './LoadingSpinner.css';

const LoadingSpinner = () => {
  return (
    <div className={`text-center text-danger`}>
      <div className={`spinner-border`} role="status" >
        <span className={`visually-hidden`}>Loading...</span>
      </div>
    </div>
  )
}

export default LoadingSpinner;