import { render, screen } from '@testing-library/react';
import PokemonModal from './PokemonModal';

describe('PokemonModal component', () => {
    test('renders pokemonDetail modal with correct information', () => {
        const show = true;
        const pokemonDetail = { name: 'ayoub', id: 1, base_experience: 100 };
        const setShowPokemonModal = () => { }
        
        render(<PokemonModal show={show} pokemonDetail={pokemonDetail}
            setShowPokemonModal={setShowPokemonModal} />);

        const pokemonNameElement = screen.getByText(pokemonDetail.name);
        const pokemonIdElement = screen.getByText(pokemonDetail.id);
        const pokemonBaseExperienceElement = screen.getByText(pokemonDetail.base_experience);

        expect(pokemonNameElement).toBeInTheDocument();
        expect(pokemonIdElement).toBeInTheDocument();
        expect(pokemonBaseExperienceElement).toBeInTheDocument()
    });
})
