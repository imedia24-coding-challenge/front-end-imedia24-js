import { Table } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

const PokemonModal = ({ show, pokemonDetail,setShowPokemonModal }) => {
    const onHideHandler=()=>{
        setShowPokemonModal(false)
    }
    return (
        <Modal
            show={show}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            onHide={onHideHandler}
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {pokemonDetail?.name}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Table>
                    <tbody>
                        <tr>
                            <td>Base experience</td>
                            <td>{pokemonDetail?.base_experience}</td>
                        </tr>
                        <tr>
                            <td>Id</td>
                            <td>{pokemonDetail?.id}</td>
                        </tr>
                        <tr>
                            <td>Height</td>
                            <td>{pokemonDetail?.height}</td>
                        </tr>
                    </tbody>
                </Table>

            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHideHandler}>Close</Button>
            
            </Modal.Footer>
        </Modal>

    );
}

export default PokemonModal;