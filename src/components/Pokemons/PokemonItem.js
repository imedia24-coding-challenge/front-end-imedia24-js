import './PokemonItem.css'
const PokemonItem = ({ pokemon,onClickPokemonDetailsHandler }) => {

    return (
        <div className="col grid-item">
            <div className={`card`}>
                <div className={`card-body`}>
                    <h3 className={`card-title`}> {pokemon.name}</h3>
                    <button className={`btn btn-primary btn-show-details`}
                     onClick={()=>onClickPokemonDetailsHandler(pokemon.url)}>
                        details
                    </button>
                </div>
            </div>
        </div>

    )
};

export default PokemonItem;