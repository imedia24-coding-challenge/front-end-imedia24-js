import { useState } from "react";
import PokemonItem from "./PokemonItem";


const Pokemons = ({ pokemons,onClickPokemonDetailsHandler }) => {
    return <>
        {pokemons.map((item, index) => {
            return (<div>
                <PokemonItem pokemon={item} key={index}
                 onClickPokemonDetailsHandler={onClickPokemonDetailsHandler} />
            </div>
            )
        })}
    </>

}

export default Pokemons;