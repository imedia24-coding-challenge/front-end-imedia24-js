import LoadingSpinner from '../Shared/LoadingSpinner/LoadingSpinner';
import Pokemons from './Pokemons';

import React, { useEffect, useState } from 'react';
import './PokemonContainer.css';
import PokemonModal from './PokemonModal';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import InfiniteScroll from 'react-infinite-scroll-component';
import apiService from '../../utils/apiService.js'
const BASE_URL = process.env.REACT_APP_BASE_URL;

const PokemonContainer = () => {
    const [pokemons, setPokemons] = useState([]);
    const [pokemonDetail, setPokemonDetail] = useState();
    const [nextPokemonsUrl, setNextPokemonsUrl] = useState(`${BASE_URL}/pokemon/`)


    const [error, setError] = useState(null);
    const [showPokemonModal, setShowPokemonModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const onClickPokemonDetailsHandler = (url) => {
        fetchPokemonDetails(url)
    }

    const fetchData = async () => {
        if (nextPokemonsUrl === '') {
            toast("There are no more pokemons to fetch")
            return
        }
        setIsLoading(true);
        setError(null);
        try {
            const data = await apiService(nextPokemonsUrl)
            setPokemons(pokemons.concat(data.results));
            setNextPokemonsUrl(data.next);

        } catch (error) {
            setError(error);
            toast(error)
        } finally {
            setIsLoading(false);
        }
    };

    useEffect(() => {
        fetchData()
    }, []);

    const fetchPokemonDetails = async (url) => {
        setIsLoading(true);
        setError(null);
        try {
            const data = await apiService(url);
            console.trace("data", data);
            setPokemonDetail(data)
            setShowPokemonModal(true)

        } catch (error) {
            setError(error);
            toast(error)
        } finally {
            setIsLoading(false);
        }
    }

    return (
        <>
            <h1 className='text-info d-flex justify-content-center mt-2'>Pokemon List</h1>
            <div>
                <ToastContainer />
            </div>

            <PokemonModal show={showPokemonModal} pokemonDetail={pokemonDetail}
                setShowPokemonModal={setShowPokemonModal} />


            <InfiniteScroll
                dataLength={pokemons.length}
                next={fetchData}
                hasMore={nextPokemonsUrl !== ''}
                loader={isLoading && <LoadingSpinner />}
                >
                <div className='grid-container'>
                    <Pokemons pokemons={pokemons} 
                    onClickPokemonDetailsHandler={onClickPokemonDetailsHandler} />
                </div>
            </InfiniteScroll>
            {error && toast(error.message)}
        </>

    )
}

export default PokemonContainer;