const  apiService = (url, options = {}) => {
    return fetch(url, options)
        .then(response => {
            // handle http status

            return response.json();
        })
        .then(result => {
            console.debug(`result call api with url=${url} , option=${options} :`, result)
            return result;
        }).catch(error => {
            console.error(`error call api with url=${url} , option=${options} :`, result)
            throw new Exception(error)
        })
}

export default apiService;