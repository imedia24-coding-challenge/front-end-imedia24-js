import { render, screen } from '@testing-library/react';
import App from './App';

describe('App component',()=>{
  test('test page title if exist', () => {
    render(<App />);
    const textElement = screen.getByText(/Pokemon List/i);
    expect(textElement).toBeInTheDocument();
  });
})

