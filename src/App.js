import './App.css';
import PokemonContainer from './components/Pokemons/PokemonContainer';

import React from 'react';


const App = () => {
  return (
    <div className='container'>
      <PokemonContainer></PokemonContainer>
    </div>
  )
}

export default App;
